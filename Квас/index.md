---
title: Квас
---

![10.jpg](10.jpg)

  * Вода 10л
  * хлеб 1кг
  * сахар 250г
  * пол столовой (10г) ложки сухих дрожжей
  * изюм 50г

  * 1) разрезать и засушить на 2\3 газа духовки (180°).
  * 2) вскипятить воду, засыпать сухари, хмель, ждать 3 часа
  * 3) Убрать мякоть. Засыпать сахар и дрожжи. Ждать 6 часов.
  * 4) разлить, добавить ложку сахара, ждать 3 дня.

Хлеб, одна буханка, квадратный:

![1.jpg](1.jpg)

Режем не мелко:

![2.jpg](2.jpg)

Сушим на 2\3 газа (180°), 70-80 минут, чтобы некоторые кусочки немного подгорели. Это придаст более насыщенный цвет и вкус.

![3.jpg](3.jpg)

Засыпаем сухари в только что вскипяченную воду:

![4.jpg](4.jpg)

Даем остыть до нормальной температуры (45 градусов, около 3 часов):

![5.jpg](5.jpg)

Процеживаем, убираем хлеб, который потом пойдет на корм уткам или другим птичкам. Добавляем дрожжи, сахар, мед, перемешиваем.

![6.jpg](6.jpg)

На следующий день (12 часов), квас забродил, запах отличный, пены много. Процеживаем и разливаем по банкам. Можно разливать только по банкам со слабой крышкой пропускающий давление - иначе они взорвутся. Если разливать в пластиковую тару для газировки необходимо дождаться полного окончания брожения (убедиться можно резиновой перчаткой надетой на тару).

![7.jpg](7.jpg)

Положите немного сахара, чтобы подкормить дрожжи и процесс брожения продолжался.

![8.jpg](8.jpg)

Плотно закрыть. Можно ставить в холодное место.

![9.jpg](9.jpg)
